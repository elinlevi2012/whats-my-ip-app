# Whats my IP App

Want to deploy an app which helps to find out your public IP? You should use this Helm Chart :wink:

## Requirements
1. K8s Cluster
2. Installed Helm 3
3. Helm configured with k8s cluster's context


## Let's start

First, you should install the helm chart.

```
helm install discovered ./whats-my-ip
```
![image0](images/services.png)

To access the app you should enter the external IP in your favourite web page and you are good to go!

![image1](images/IP.png)

## How it works?
The helm will deploy two different pods:
- Alpine based flask app which responsible for retriving your public IP
- Nginx that proxies your request and returns the IP in html web page

## What's next?
The next version will have a route (for openshift deployments) or ingress object, which will make it possible to use fqdn via web browser instead of the IP address.

Stay tuned!
