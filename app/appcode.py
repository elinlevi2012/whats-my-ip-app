#! /usr/local/bin/python3.7
from flask import Flask, render_template , request, jsonify

app = Flask(__name__)

@app.route("/")
def new_request():

  if request.environ.get('HTTP_X_FORWARDED_FOR') is None:
    data = request.environ['REMOTE_ADDR']
  else:
    data = request.environ['HTTP_X_FORWARDED_FOR']
  return render_template("index.html", message=data);


if __name__ == '__main__':
  app.run(debug=True, host='0.0.0.0')
